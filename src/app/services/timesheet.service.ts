import { Injectable } from "@angular/core";
import { environment } from "src/environments/environment.prod";
import { HttpClient, HttpHeaders } from "@angular/common/http";

@Injectable({
  providedIn: "root"
})
export class TimesheetService {
  private baseapi = environment.apiUrl;

  constructor(private http: HttpClient) {}

  getWeeklyTimesheet(employeeId: number, week: Number) {
    return this.http.get(
      this.baseapi +
        "/Timesheet/GetWeeklyTimesheet/" +
        employeeId +
        "/" +
        week
    );
  }

  saveTimesheet(timeSheetModel: any)
  {
    let timeSheetJson: String = JSON.stringify(timeSheetModel);
    return this.http.post(this.baseapi + "/Timesheet", timeSheetJson,{
      headers: new HttpHeaders({
          'Content-Type': 'application/json'
      })
  });
  }
}

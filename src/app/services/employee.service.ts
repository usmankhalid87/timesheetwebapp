import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment.prod';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  private baseapi = environment.apiUrl;
    constructor(private http: HttpClient) { }

    getAllEmployees() {
        return this.http.get(this.baseapi + "/employee/getall");
    }

    getEmployeeWeekEffortList() {
      return this.http.get(this.baseapi + "/employee/GetEmployeeWeekEffortList");
  }
}

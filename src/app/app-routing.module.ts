import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { EmployeeListComponent } from './employee/employee.component';
import { EmployeetimesheetComponent } from "./employee-timesheet/employee-timesheet.component";

const routes: Routes = [
  { path: '', component: EmployeeListComponent },
  { path: 'employee-timesheet/:id', component: EmployeetimesheetComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports:[RouterModule]
})
export class AppRoutingModule {}
import { ITimesheetRecord } from './ITimesheetRecord';
import { IEmployeeTimesheet } from './IEmployeeTimesheet';
import { TimesheetService } from "./../services/timesheet.service";
import { Component, OnInit } from "@angular/core";
import { EmployeeService } from "../services/employee.service";
import { ActivatedRoute, Router } from "@angular/router";
import { TaskService } from "../services/task.service";
import { forkJoin } from "rxjs";
import { ToastrManager } from "ng6-toastr-notifications";
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';

@Component({
  selector: "app-employeetimesheet",
  templateUrl: "./employee-timesheet.component.html",
  styleUrls: ["./employee-timesheet.component.scss"]
})
export class EmployeetimesheetComponent implements OnInit {
  
  tasks: any;
  employees: any;
  timesheetForm : FormGroup;

  constructor(
    private _formBuilder : FormBuilder,
    private _employeeService: EmployeeService,
    private _taskService: TaskService,
    private _timesheetService: TimesheetService,
    private route: ActivatedRoute,
    private router: Router,
    public toastr: ToastrManager
  ) {}

  ngOnInit() {

    this.timesheetForm = this._formBuilder.group(
      {
        employeeId : [null, [Validators.required]],
        week : [null],
        fromDate : [null],
        toDate : [null],
        timesheetRecords : this._formBuilder.array([this.addTimesheetRecord()])
      }
    ); 

    this.route.params.subscribe(val => {
      this.loadEmployeeTimesheet(val["id"], 0);
    });
  }

  addTimesheetRecord()
  {
    return this._formBuilder.group({
        taskId : [null,[Validators.required]],

        sundayEffort : [null],
        mondayEffort : [null],
        tuesdayEffort : [null],
        wednesdayEffort : [null],
        thursdayEffort : [null],
        fridayEffort : [null],
        saturdayEffort : [null],
    });
  }

  get timesheetRecords() : FormArray{
    return this.timesheetForm.get('timesheetRecords') as FormArray;
  }

  addNewRow()
  {
    this.timesheetRecords.push(this.addTimesheetRecord());
  }

  loadEmployeeTimesheet(employeeId:any, week : any): void {

    let employeeObservable = this._employeeService.getAllEmployees();
    let taskObservable = this._taskService.getAllTasks();
    let timesheetObservable = this._timesheetService.getWeeklyTimesheet(
      employeeId,
      week
    );

    forkJoin([
      employeeObservable,
      taskObservable,
      timesheetObservable
    ]).subscribe(([response1, response2, response3]) => {
      this.employees = response1;
      this.tasks = response2;

      this.bindTimesheet(response3 as IEmployeeTimesheet);
    });
  }

  bindTimesheet(employeeTimesheet : IEmployeeTimesheet)
  {
    this.timesheetForm.patchValue({
      employeeId : employeeTimesheet.employeeId,
      week : employeeTimesheet.week,
      fromDate : employeeTimesheet.fromDate,
      toDate : employeeTimesheet.toDate
    });

    this.timesheetForm.setControl("timesheetRecords", 
    this.setExistingRecords(employeeTimesheet.timesheetRecords));
  }

  setExistingRecords(records : ITimesheetRecord[]) : FormArray
  {
    var formArray = new FormArray([]);

    records.forEach(r => {
      formArray.push(this._formBuilder.group(
          {
            taskId : r.taskId,

            sundayEffort : r.sundayEffort,

            mondayEffort : r.mondayEffort,

            tuesdayEffort : r.tuesdayEffort,
            

            wednesdayEffort : r.wednesdayEffort,


            thursdayEffort : r.thursdayEffort,


            fridayEffort : r.fridayEffort,


            saturdayEffort : r.saturdayEffort,
          }
        )
      )
    });
    if(formArray.length === 0)
      formArray.push(this.addTimesheetRecord());

    return formArray;
  }

  onSelectedEmployeeChange() {
    this.router.navigateByUrl("/employee-timesheet/" + this.timesheetForm.value.employeeId);
  }

  onNextClick() {
    this.timesheetForm.value.week = this.timesheetForm.value.week + 1;
    this.loadEmployeeTimesheet(this.timesheetForm.value.employeeId, this.timesheetForm.value.week);
  }

  onPreviousClick() {
    this.timesheetForm.value.week = this.timesheetForm.value.week - 1;
    this.loadEmployeeTimesheet(this.timesheetForm.value.employeeId, this.timesheetForm.value.week);
  }

  onFormSubmit() {
    var model = {
      week: this.timesheetForm.value.week,
      employeeId: this.timesheetForm.value.employeeId,
      timesheetRecords : this.timesheetForm.value.timesheetRecords
    };

    this._timesheetService.saveTimesheet(model).subscribe(
      response => {
        this.toastr.successToastr("Data successfully saved.", "Success");
      },
      error => {
        this.toastr.errorToastr("An Error occured while executing.", "Error");
      }
    );
  }

  addDays(date, days) {
    var result = new Date(date);
    result.setDate(result.getDate() + days);
    return result;
  }
}

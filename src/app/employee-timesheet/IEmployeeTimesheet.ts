import { ITimesheetRecord } from './ITimesheetRecord';

export interface IEmployeeTimesheet
{
    employeeId : any;
    week:any;
    fromDate:any;
    toDate:any;
    timesheetRecords : ITimesheetRecord[]
}
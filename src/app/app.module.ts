import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { HttpClientModule } from "@angular/common/http";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { ToastrModule } from "ng6-toastr-notifications";

import { AppRoutingModule } from './app-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppComponent } from "./app.component";
import { EmployeeService } from "./services/employee.service";
import { TaskService } from "./services/task.service";
import { TimesheetService } from './services/timesheet.service';
import { EmployeetimesheetComponent } from "./employee-timesheet/employee-timesheet.component";
import { EmployeeListComponent } from "./employee/employee.component";

@NgModule({
  declarations: [AppComponent, EmployeeListComponent, EmployeetimesheetComponent],
  imports: [
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    ToastrModule.forRoot()
  ],
  providers: [EmployeeService, TaskService, TimesheetService],
  bootstrap: [AppComponent]
})
export class AppModule { }
